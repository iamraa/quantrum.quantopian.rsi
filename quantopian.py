# This example algorithm uses the Relative Strength Index indicator as a buy/sell signal.
# When the RSI is over 70, a stock can be seen as overbought and it's time to sell.
# When the RSI is below 30, a stock can be seen as oversold and it's time to buy.

import talib


# Setup our variables
def initialize(context):
    # Indices: 'DIA', 'QQQ', 'SPY', 'IWM'
    # Sectors: 'XLY', 'XLK', 'XLI', 'XLB', 'XLE', 'XLP', 'XLV', 'XLU', 'XLF', 'XLRE'
    context.stocks = symbols('SPY')
    #context.stocks = symbols('XLY', 'XLK', 'XLI', 'XLB', 'XLE', 'XLP', 'XLV', 'XLU', 'XLF', 'XLRE')
    context.RSI_PERIOD = 5
    context.RSI_LOW, context.RSI_HIGH = 30, 70
    context.CLOSE_PERIOD = 0  # any positive number
    context.hold_symbols = dict()

    schedule_function(rsi_base, date_rules.every_day(), time_rules.market_open())


def rsi_base(context, data):
    """
    Стандартная стратегия торговли.
    От 30 покупаем, от 70 продаем.
    На входе в зоны перекупленности/перепроданности.
    """
    can_trade = data.can_trade(context.stocks)

    # Load historical data for the stocks
    prices = data.history(context.stocks, 'price', 500, '1d')

    trades = {
        'long': [],
        'short': []
    }
    hold = {
        'long': [],
        'short': []
    }

    # Loop through our list of stocks
    for stock in context.stocks:
        # skip stocks
        if not can_trade[stock]:
            continue

        # calculate RSI
        arr_rsi = talib.RSI(prices[stock], timeperiod=context.RSI_PERIOD)
        # use RSI of daily close
        rsi = arr_rsi[-2]
        rsi_prev = arr_rsi[-3]

        # signal to open on enter
        long_allow = rsi < context.RSI_LOW and rsi_prev > context.RSI_LOW
        short_allow = rsi > context.RSI_HIGH and rsi_prev < context.RSI_HIGH

        # signal to open on exit
        #long_allow = rsi > context.RSI_LOW and rsi_prev < context.RSI_LOW
        #short_allow = rsi < context.RSI_HIGH and rsi_prev > context.RSI_HIGH

        if long_allow:
            trades['long'].append(stock)
        elif short_allow:
            trades['short'].append(stock)
            pass
        else:
            #log.warn('No signal for {0}'.format(stock.symbol))
            pass

        # signal to hold
        long_hold = rsi < context.RSI_HIGH # hold under high
        short_hold = rsi > context.RSI_LOW # hold above low

        # hold only opened positions
        if context.portfolio.positions[stock].amount > 0:
            # stop at 15/85
            #if long_hold and rsi < context.RSI_HIGH+15:
            if long_hold:
                hold['long'].append(stock)
            #elif short_hold and rsi > context.RSI_LOW-15:
            elif short_hold:
                hold['short'].append(stock)

        if len(context.stocks) < 3:
            record(**{'RSI_' + stock.symbol: rsi})

    # open positions for the whole capital
    total_long = len(set(trades['long'] + hold['long']))
    total_short = len(set(trades['short'] + hold['short']))
    coef_long = 1./(total_long if total_long else 1)  # long is positive
    coef_short = 1./(total_short if total_short else 1)  # short is negative

    # disable direction
    #trades['short'] = []
    #hold['short'] = []

    for stock in context.stocks:
        # decrease hold period
        if stock in context.hold_symbols:
            context.hold_symbols[stock] -= 1
        else:
            context.hold_symbols[stock] = 0

        # buy to long stock
        if stock in trades['long']:
            order_target_percent(stock, coef_long)
            context.hold_symbols[stock] = context.CLOSE_PERIOD
            print('long', stock.symbol)

        # sell to short stock
        elif stock in trades['short']:
            # negative percent
            order_target_percent(stock, -1 * coef_short)
            context.hold_symbols[stock] = context.CLOSE_PERIOD
            print('short', stock.symbol)

        # close position
        elif (context.portfolio.positions[stock].amount
              and stock not in hold['long'] and stock not in hold['short']):
            order_target_percent(stock, 0)
            print('close', stock.symbol)
            pass

        elif (context.portfolio.positions[stock].amount
              and context.CLOSE_PERIOD and context.hold_symbols[stock] <= 0):
            order_target_percent(stock, 0)
            print('close by timeout', stock.symbol)

        if len(context.stocks) < 3:
            record(**{stock.symbol: context.portfolio.positions[stock].amount})